#![feature(lang_items, asm)]
#![crate_type = "staticlib"]
#![no_std]
#![no_main]
#![allow(dead_code)]

use numtoa::NumToA;

mod uart;

mod timer;

mod control;

mod clock;

mod mem_utils;

mod i2c;

mod lcd;

const GPIO1_BASE: usize = 0x4804C000;
const GPIO_OE: usize = 0x134;
const GPIO_SETDATAOUT: usize = 0x194;
const GPIO_SETDATACLEAR: usize = 0x190;

const CM_PER_BASE: usize = 0x44e00000;
const CM_PER_GPIO1: usize = 0xAC;

const UNDEFINED_VECTOR: usize = 0x4030CE24;

#[no_mangle]
#[link_section = ".start"]
pub fn _start() -> ! {
    unsafe {
        let handle = interupt as usize;
        mem_write!(UNDEFINED_VECTOR, handle);
    }
    main();
}

struct Peripherals {
    uart0: Option<uart::Uart>,
    lcd: Option<lcd::LCD>,
    uart4: Option<uart::Uart>,
    timer0: Option<timer::Timer>,
}
impl Peripherals {
    fn take_uart0(&mut self) -> Option<uart::Uart> {
        let p = core::mem::replace(&mut self.uart0, None);
        p
    }
    fn take_uart4(&mut self) -> Option<uart::Uart>{
        let p = core::mem::replace(&mut self.uart4, None);
        p
    }
    fn take_timer0(&mut self) -> Option<timer::Timer> {
        let p = core::mem::replace(&mut self.timer0, None);
        p
    }

    fn take_lcd(&mut self) -> Option<lcd::LCD>{
        let p = core::mem::replace(&mut self.lcd, None);
        p
    }
}

static mut PERIPHERALS: Peripherals = Peripherals {
    uart0: Some(uart::Uart {device: uart::UartDevice::UART0}),
    uart4: Some(uart::Uart {device: uart::UartDevice::UART4}),
    lcd: Some(lcd::LCD{}),
    timer0: Some(timer::Timer {}),
};

#[no_mangle]
pub extern "C" fn interupt() -> ! {
    unsafe {
        mem_write!(CM_PER_BASE + CM_PER_GPIO1, 1 << 18 | 2);
        let oe = mem_read!(GPIO1_BASE + GPIO_OE);
        mem_write!(GPIO1_BASE + GPIO_OE, oe & !(1 << 21));
        loop {
            mem_write!(GPIO1_BASE + GPIO_SETDATAOUT, 1 << 21);
            for _x in 0..50000 {
                asm!("nop");
            }
            mem_write!(GPIO1_BASE + GPIO_SETDATACLEAR, 1 << 21);
            for _x in 0..50000 {
                asm!("nop");
            }
        }
    }
}

#[no_mangle]
pub fn main() -> ! {
    unsafe {
        let mut uart0 = PERIPHERALS.take_uart0().unwrap();
        let mut uart4 = PERIPHERALS.take_uart4().unwrap();
        let mut timer0 = PERIPHERALS.take_timer0().unwrap();
        let mut lcd = PERIPHERALS.take_lcd().unwrap();





        uart0.init();

      
        for _x in 0..500000 {
            asm!("nop");
        }

        uart0.puts(b"Test1\n");
       
        timer0.init();
        lcd.init(&timer0, &uart0);

        uart0.puts(b"Test2\n");
        
        


        mem_write!(CM_PER_BASE + CM_PER_GPIO1, 1 << 18 | 2);
        let oe = mem_read!(GPIO1_BASE + GPIO_OE);
        mem_write!(GPIO1_BASE + GPIO_OE, oe & !(1 << 21));

        
        
        


        loop {
            mem_write!(GPIO1_BASE + GPIO_SETDATAOUT, 1 << 21);
            timer0.wait(100);
            mem_write!(GPIO1_BASE + GPIO_SETDATACLEAR, 1 << 21);
            timer0.wait(100);
          
        }
    }
}

#[lang = "eh_personality"]
#[no_mangle]
pub extern "C" fn eh_personality() {}

#[panic_handler]
fn my_panic(pi: &::core::panic::PanicInfo) -> ! {
    let panic_uart = uart::Uart {device: uart::UartDevice::UART0};
    unsafe {
        mem_write!(CM_PER_BASE + CM_PER_GPIO1, 1 << 18 | 2);
        let oe = mem_read!(GPIO1_BASE + GPIO_OE);
        mem_write!(GPIO1_BASE + GPIO_OE, oe & !(1 << 21));

        mem_write!(CM_PER_BASE + CM_PER_GPIO1, 1 << 18 | 2);
        panic_uart.init();
        for _x in 0..500000 {
            asm!("nop");
        }
        let mut buffer = [0u8; 20];
        panic_uart.puts(b"A panic has occured at ");
        match pi.location() {
            Some(loc) => {
                panic_uart.puts(loc.file().as_bytes());
                panic_uart.putc(b':');

                panic_uart.puts(loc.line().numtoa(10, &mut buffer))
            }
            None => panic_uart.puts(b"{unknown}"),
        }
        panic_uart.putc(b' ');
        if let Some(s) = pi.payload().downcast_ref::<&str>() {
            panic_uart.puts(s.as_bytes());
        } else {
            panic_uart.puts(b"\"panic occurred\"");
        }
        panic_uart.putc(b'\n');
    }

    loop {}
}

#[no_mangle]
pub extern "C" fn __aeabi_unwind_cpp_pr0() {}
