use crate::{clock::*, control::*, mem_write, mem_write_byte, timer::Timer, uart::*};




const LCD_BASE: usize = 0x4830E000;
const LCD_CTRL: usize = LCD_BASE + 0x4;
const LCD_LIDD_CTRL: usize = LCD_BASE + 0xc;
const LCD_LIDD_CS0_ADDR: usize = LCD_BASE + 0x14;


pub struct LCD{

}
impl LCD{
    pub unsafe fn init(&mut self, timer: &Timer, dbg: &Uart){
        dbg.puts(b"lcd_debug1\n\r");
        let data4 = IOPin::new(PinRegister::conf_lcd_data4);
        data4.set(Some(PinMode::Mode0), Some(true), Some((PullType::Pulldown, false)), Some(SlewMode::Fast));
        let data5 = IOPin::new(PinRegister::conf_lcd_data5);
        data5.set(Some(PinMode::Mode0), Some(true), Some((PullType::Pulldown, false)), Some(SlewMode::Fast));
        let data6 = IOPin::new(PinRegister::conf_lcd_data6);
        data6.set(Some(PinMode::Mode0), Some(true), Some((PullType::Pulldown, false)), Some(SlewMode::Fast));
        let data7 = IOPin::new(PinRegister::conf_lcd_data7);
        data7.set(Some(PinMode::Mode0), Some(true), Some((PullType::Pulldown, false)), Some(SlewMode::Fast));

        let vsync = IOPin::new(PinRegister::conf_lcd_vsync);
        vsync.set(Some(PinMode::Mode0), Some(true), Some((PullType::Pulldown, true)), Some(SlewMode::Fast));
        let hsync = IOPin::new(PinRegister::conf_lcd_hsync);
        hsync.set(Some(PinMode::Mode0), Some(true), Some((PullType::Pulldown, true)), Some(SlewMode::Fast));

        let strobe = IOPin::new(PinRegister::conf_lcd_ac_bias_en);
        strobe.set(Some(PinMode::Mode0), Some(true), Some((PullType::Pulldown, false)), Some(SlewMode::Fast));
    

        dbg.puts(b"lcd_debug2\n\r");
    
        ClockCtrl::new(CM_PER_L3S_CLKSTCTRL).unwrap().set_state(ClockCtrlState::Wakeup);
    
        ClockCtrl::new(CM_PER_L3_CLKSTCTRL).unwrap().set_state(ClockCtrlState::Wakeup);

        ClockModule::new(CM_PER_L3_INSTR_CLKCTRL).unwrap().enable();

        ClockModule::new(CM_PER_L3_CLKCTRL).unwrap().enable();

        ClockCtrl::new(CM_PER_OCPWP_L3_CLKSTCTRL).unwrap().set_state(ClockCtrlState::Wakeup);

        ClockCtrl::new(CM_PER_L4HS_CLKSTCTRL).unwrap().set_state(ClockCtrlState::Wakeup);

        ClockModule::new(CM_PER_L4LS_CLKCTRL).unwrap().enable();

        dbg.puts(b"lcd_debug3\n\r");


        //set controller to LIDD mode
        mem_write!(LCD_CTRL, 0);
        dbg.puts(b"lcd_debug3.5\n\r");
        //Hitachi mode
        let LIDD_CTRL = 0b100;
        dbg.puts(b"lcd_debug4\n\r");
        mem_write!(LCD_LIDD_CTRL, LIDD_CTRL);
        timer.wait(20);
        dbg.puts(b"lcd_debug5\n\r");
        mem_write_byte!(LCD_LIDD_CS0_ADDR, 0b0011<<4);
        timer.wait(5);
        dbg.puts(b"lcd_debug6\n\r");
        mem_write_byte!(LCD_LIDD_CS0_ADDR, 0b0011<<4);
        dbg.puts(b"lcd_debug7");
        timer.wait(1);
        dbg.puts(b"lcd_debug8");
        mem_write_byte!(LCD_LIDD_CS0_ADDR, 0b0011<<4);
        timer.wait(1);
        mem_write_byte!(LCD_LIDD_CS0_ADDR, 0b0011<<4);
        timer.wait(1);

        mem_write_byte!(LCD_LIDD_CS0_ADDR, 0b0010<<4);
        timer.wait(1);
        mem_write_byte!(LCD_LIDD_CS0_ADDR, 0b0010<<4);
        timer.wait(1);
        mem_write_byte!(LCD_LIDD_CS0_ADDR, 0b1000<<4);
        timer.wait(1);

        mem_write_byte!(LCD_LIDD_CS0_ADDR, 0b0000<<4);
        timer.wait(1);
        mem_write_byte!(LCD_LIDD_CS0_ADDR, 0b1000<<4);
        timer.wait(1);

        mem_write_byte!(LCD_LIDD_CS0_ADDR, 0b0000<<4);
        timer.wait(1);
        mem_write_byte!(LCD_LIDD_CS0_ADDR, 0b0001<<4);
        timer.wait(1);

        mem_write_byte!(LCD_LIDD_CS0_ADDR, 0b0000<<4);
        timer.wait(1);
        mem_write_byte!(LCD_LIDD_CS0_ADDR, 0b0110<<4);
        timer.wait(1);



        mem_write_byte!(LCD_LIDD_CS0_ADDR, 0b0000<<4);
        timer.wait(1);
        mem_write_byte!(LCD_LIDD_CS0_ADDR, 0b1111<<4);
        timer.wait(1);


        dbg.puts(b"lcd_debug9\n\r");
        











        

    


    }
}