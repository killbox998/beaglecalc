const DM_TIMER0: usize = 0x44E05000;

use super::*;
use clock::*;

#[allow(dead_code)]
#[allow(non_camel_case_types)]
#[derive(Clone, Copy, Debug)]
#[repr(usize)]
pub enum TimerRegister {
    TIDR = 0x0,
    TIOCP_CFG = 0x10,
    IRQ_EOI = 0x20,
    IRQSTATUS_RAW = 0x24,
    IRQSTATUS = 0x28,
    IRQENABLE_SET = 0x2c,
    IRQENABLE_CLR = 0x30,
    IRQWAKEEN = 0x34,
    TCLR = 0x38,
    TCRR = 0x3C,
    TLDR = 0x40,
    TTGR = 0x44,
    TWPS = 0x48,
    TMAR = 0x4c,
    TCAR1 = 0x50,
    TSICR = 0x54,
    TCAR2 = 0x58,
}

const CKM_PER: usize = 0x44E00000;
const CKM_WKUP: usize = 0x44E00400;

const CKM_PER_L4HS_CLKSTCTRL: usize = 0x11C;
const CKM_WKUP_CLKSTCTRL: usize = 0x0;
const CKM_WKUP_TIMER0_CLKCTRL: usize = 0x10;

pub struct Timer {}

impl Timer {
    pub unsafe fn init(&self) {
        //Start a software forced wake-up on the always on clock domain

        ClockCtrl::new(CM_WKUP_CLKSTCTRL)
            .unwrap()
            .set_state(ClockCtrlState::Wakeup);

        //Start a software forced wake-up on the L4 slow clock domain

        ClockCtrl::new(CM_PER_L4HS_CLKSTCTRL).unwrap().set_state(ClockCtrlState::Wakeup);

        ClockModule::new(CM_WKUP_TIMER0_CLKCTRL).unwrap().enable();

        // mem_write!(DM_TIMER0 + TimerRegister::TCLR as usize,  )
        mem_write!(DM_TIMER0 + TimerRegister::TCLR as usize, 0b111111);
    }
    pub unsafe fn get_counter(&self) -> usize {
        mem_read!(DM_TIMER0 + TimerRegister::TCRR as usize)
    }
    pub unsafe fn wait(&self, millis: usize) {
        let count = millis / 8;
        let start = self.get_counter();
        let end = start + count;
        while self.get_counter() < end {}
    }
}
