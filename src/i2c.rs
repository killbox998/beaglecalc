use crate::control::*;

use crate::clock::*;

struct I2C {}

impl I2C {
    pub unsafe fn init(&self) {
        let i2c0_sda = IOPin::new(PinRegister::conf_i2c0_sda);
        i2c0_sda.set(
            Some(PinMode::Mode0),
            Some(true),
            Some((PullType::Pullup, true)),
            Some(SlewMode::Slow),
        );

        let i2c0_scl = IOPin::new(PinRegister::conf_i2c0_scl);
        i2c0_scl.set(
            Some(PinMode::Mode0),
            Some(true),
            Some((PullType::Pullup, true)),
            Some(SlewMode::Slow),
        );

        //Start a software forced wake-up on the always on clock domain

        ClockCtrl::new(CM_WKUP_CLKSTCTRL)
            .unwrap()
            .set_state(ClockCtrlState::Wakeup);

        //Start a software forced wake-up on the L4 slow clock domain

        ClockModule::new(CM_PER_L4HS_CLKSTCTRL).unwrap().enable();

        ClockModule::new(CM_WKUP_I2C0_CLKCTRL).unwrap().enable();
    }
}
