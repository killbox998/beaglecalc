#[macro_export]
macro_rules! mem_write {
    ($x:expr, $y:expr) => {
        core::ptr::write_volatile(($x) as *mut usize, $y);
    };
}
#[macro_export]
macro_rules! mem_read {
    ($x:expr) => {
        core::ptr::read_volatile(($x) as *mut usize);
    };
}

#[macro_export]
macro_rules! mem_write_byte {
    ($x:expr, $y:expr) => {
        core::ptr::write_volatile(($x) as *mut u8, $y);
    };
}
#[macro_export]
macro_rules! mem_read_byte {
    ($x:expr) => {
        core::ptr::read_volatile(($x) as *mut u8);
    };
}
